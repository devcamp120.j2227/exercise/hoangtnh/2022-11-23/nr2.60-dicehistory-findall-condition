//import thư viên mongoose
const mongoose = require("mongoose");
//import voucher history model 
const voucherHisModel = require("../models/voucherHistoryModel");
//function create voucher history
const createVoucherHistory = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    //B2: validate dữ liệu
    //B3: gọi model tạo dữ liệu
    const newVoucherHistory = {
        _id: mongoose.Types.ObjectId(),
        user: mongoose.Types.ObjectId(),
        voucher: mongoose.Types.ObjectId(),
        createAt: body.createAt,
        updateAt: body.updatedAt
    }
   
    voucherHisModel.create(newVoucherHistory, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new voucher history successfully",
            data: data
        })
    })
}
//function get all voucher histories
const getAllVoucherHistory = (request, response) => {
    voucherHisModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all voucher histories successfully",
            data: data
        })
    })
}
//function get voucher history by id
const getVoucherHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const voucherHisId = request.params.voucherHisId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHisId)){
        return response.status(400).json({
            status:"Bad Request",
            message: "Voucher history id không hợp lệ"
        })
    }
    //B3: gọi model chưa id prize 
    voucherHisModel.findById(voucherHisId,(error,data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get voucher history by id ${voucherHisId} successfully`,
            data: data
        })
    })
}
//function update voucher history by id
const updateVoucherHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu'
    const voucherHisId = request.params.voucherHisId;
    const body = request.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHisId)){
        return response.status(400).json({
            status: "Bad request",
            message: "Voucher History Id không đúng"
        })
    }
    //B3: gọi model chưa id để tìm và update dữ liệu
    const updateVoucherHistory = {};
    if(body.user !== undefined){
        updateVoucherHistory.user = mongoose.Types.ObjectId();
    }
    if(body.voucher !== undefined){
        updateVoucherHistory.prize = mongoose.Types.ObjectId();
    }
    voucherHisModel.findByIdAndUpdate(voucherHisId, updateVoucherHistory, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Update Voucher History successfully",
            data: data
        })
    })
}
//function delete voucher history by id
const deleteVoucherHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const voucherHisId = request.params.voucherHisId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHisId)){
        return response.status(400).json({
            status:" Bad request",
            message:"Voucher history id không đúng"
        })
    }
    //B3: gọi model chưa id cần xóa
    voucherHisModel.findByIdAndRemove(voucherHisId,(error, data) => {
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`delete Voucher History had id ${voucherHisId} successfully`
        })
    })
}
module.exports ={
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById
}